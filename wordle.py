import random

class Wordle:
    def __init__(self, word):
        self.word = word
        randomWord = random.choice(['lucky', 'butter', 'bread', 'beard', 'bacon', 'pizza'])
    def checker(self, word, randomWord):
        outputColour = []
        for index in range(len(word)):
            if word[index] == randomWord[index]:
                outputColour.append('g')
            elif word[index] in randomWord:
                outputColour.append('y')
            else:
                outputColour.append('r')
        return "".join(outputColour)
