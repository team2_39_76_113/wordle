import requests as rq
import random

DEBUG = False

class WWBot:
    with open("words_for_wordle.txt") as f:
        words = [word.strip() for word in f]
    ww_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = ww_url + "register"
    create_url = ww_url + "create"
    guess_url = ww_url + "guess"

    def __init__(self, name: str):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WWBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        create_dict = {"id": self.me, "overwrite": True}
        self.session.post(WWBot.create_url, json=create_dict)

        self.choices = WWBot.words[:]
        random.shuffle(self.choices)

    def play(self):
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WWBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        attempt = 1

        while not won and attempt < 6:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
            attempt += 1

        if won:
            print(f"heyyy !!! You cracked the code '{choice}' in {attempt} attempts.")
        else:
            print("Failed to find the secret in 6 attempts.")
        
        print("Sequence:", " -> ".join(tries))

    def update(self, choice: str, feedback: str):
        def matches_feedback(choice: str, word: str, feedback: str) -> bool:
            for i, f in enumerate(feedback):
                if f == 'G' and choice[i] != word[i]:
                    return False
                if f == 'Y' and (choice[i] == word[i] or choice[i] not in word):
                    return False
                if f == 'R' and choice[i] in word:
                    return False
            return True

        self.choices = [w for w in self.choices if matches_feedback(choice, w, feedback)]


game = WWBot("name")
game.play()

